/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elektromiska.simpleconsolefacexmppchat;

import java.util.Collection;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;

/**
 *
 * @author misi
 */
public class SimpleConsoleChat {

    private final Logger log;

    private final Connection connection;

    public SimpleConsoleChat() {

	this.log = Logger.getLogger(SimpleConsoleChat.class.getName());

	ConnectionConfiguration connectionConfiguration = new ConnectionConfiguration("chat.facebook.com", 5222);

	connectionConfiguration.setSASLAuthenticationEnabled(true);

	this.connection = new XMPPConnection(connectionConfiguration);
    }

    public void Connect() throws XMPPException {
	connection.connect();
	log.info("Csatlakozás a szerverhez...");
    }

    public void Login(String user, String password) throws XMPPException {
	connection.login(user, password);
	log.info("Bejeletkezés...");	
    }

    public void ListOnlineUser() {

	Roster roster = connection.getRoster();

	Collection<RosterEntry> entries = roster.getEntries();

	if (entries.stream().filter((entry) -> (roster.getPresence(entry.getUser()).isAvailable())).count() > 0) {
	    System.out.println("Online felhasználók: ");

	    entries.stream().filter((entry) -> (roster.getPresence(entry.getUser()).isAvailable())).forEach((entry) -> {
		System.out.println(entry.getName());
	    });
	}
	else
	    System.out.println("Nincs senki online! :(");
    }

    public void ListOnlineUserAndMess() {

	Roster roster = connection.getRoster();

	Collection<RosterEntry> rosterEntry = roster.getEntries();

	int i = 0;

	List<RosterEntry> collect = rosterEntry.stream().filter((entry) -> (roster.getPresence(entry.getUser()).isAvailable())).map((RosterEntry j) -> j).collect(Collectors.toList());

	collect.forEach((RosterEntry entry) -> {
	    System.out.println(collect.indexOf(entry) + 1 + ". " + entry.getName());
	});

	Scanner in = new Scanner(System.in);

	System.out.print("Válassz chat partnert(0 visszalépés): ");

	int userindex = in.nextInt();
	in.nextLine();

	if (userindex > 0 && userindex <= collect.size()) {
	    System.out.print("Üzenet " + collect.get(userindex - 1).getName() + "-nak: ");
	    String message = in.nextLine();
	    SendMsg(collect.get(userindex - 1).getUser(), message);
	}
    }

    public void SendMsg(String user, String message) {
	Message msg = new Message(user, Message.Type.chat);
	msg.setBody(message);
	connection.sendPacket(msg);
    }

    public void Menu() {
	System.out.print("Menü index: ");

	switch (new Scanner(System.in).nextInt()) {

	    case 0:
		Exit();
	    case 1:
		Usage();
		break;
	    case 2:
		ListOnlineUser();
		break;
	    case 3:
		ListOnlineUserAndMess();

	    default:
		System.out.println("Ismeretlen utasítás!");
	}
    }

    public void Usage() {
	System.out.println("Használat:");
	System.out.println("1. Menü");
	System.out.println("2. Online felhasználók listázása");
	System.out.println("3. Online felhasználó választása és üzenet küldése");

	System.out.println("0. Kilép");
    }

    public void Exit() {
	connection.disconnect();
	System.out.println("Bye-bye...");
	System.exit(0);
    }

    public void ReceiveMessage() {

	connection.getChatManager().addChatListener((Chat chat, boolean bln) -> {
	    chat.addMessageListener((Chat chat1, Message msg) -> {
		if (msg != null && msg.getBody() != null)
		    System.out.println("\n" + connection.getRoster().getEntries().stream().filter(i -> i.getUser().equals(msg.getFrom())).findFirst().get().getName() + " üzenete: " + msg.getBody() + "\n");
	    });
	});
    }
}

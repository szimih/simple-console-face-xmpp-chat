/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.elektromiska.simpleconsolefacexmppchat;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.XMPPException;

/**
 *
 * @author misi
 */
public class Main {

    public static void main(String[] args) throws InterruptedException {

	Scanner in = new Scanner(System.in);

	SimpleConsoleChat scc = new SimpleConsoleChat();

	try {
	    scc.Connect();

	    System.out.print("Felhasználónév: ");
	    String uname = in.nextLine();
	    
	    System.out.print("Jelszó: ");
	    String pass = in.nextLine();
	    
	    scc.Login(uname, pass);

	    Thread.sleep(1000);

	    scc.ReceiveMessage();

	    scc.Usage();

	    while (true)
		scc.Menu();

	} catch (XMPPException ex) {
	    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
	}
    }
}
